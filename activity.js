// Item 2 Solution:

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$count: "fruitsOnSale"}
])


// Item 3 Solution:

db.fruits.aggregate([
    {$match: {stock: {$gte: 20}}},
    {$count: "fruitsGreaterThanorEqualto20"}
])


// Item 4 Solution:

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", avg_price:{$avg: "$price"}}}
])


// Item 5 Solution:

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", max_price:{$max: "$price"}}}
])


// Item 6 Solution:

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:"$supplier_id", min_price:{$min: "$price"}}}
])